const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let loanSchema = new Schema({
    requestedValue: { type: Number, required: [true, 'El valor del préstamo es requerido'] },
    payDate: { type: Date, required: false },
    creditState: { type: String, unique: true, required: true },
    creditIsPay: { type: String, unique: true, required: true },
    user: { type: Schema.Types.ObjectId, ref: 'User' }
});

loanSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('Loan', loanSchema);