const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let loanSchema = new Schema({
    requestedValue: { type: Number, required: [true, 'El valor del préstamo es requerido'] },
    payDate: { type: Date, required: false },
    creditState: { type: String, required: true },
    creditIsPay: { type: String, required: true },
//     user: { type: Schema.Types.ObjectId, ref: 'User' }
});

let userSchema = new Schema({
    name: { type: String, required: [true, 'El nombre es requerido'] },
    email: { type: String, required: [true, 'El correo es requerido'] },
    documentId: { type: String, unique: true, required: [true, 'La Cédula es requerida'] },
    loan: { type: [loanSchema] },
});

userSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('User', userSchema);