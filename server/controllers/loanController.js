const express = require('express');

const app = express();

const Loan = require('../model/loans');

app.get('/loan', (req, res) => {
    Loan.find({}, (err, loan) => {
        
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            loan
        });
    });
    
});


app.post('/loan', (req, res) => {
    let body = req.body;
    
    let loan = new Loan({
        requestedValue: body.requestedValue,
        payDate: body.payDate,
        creditIsPay: body.creditIsPay,
        creditState: body.creditState,
        user: body.user
    });
    postLoad(loan, res);
});

function postLoad (loan, res) {
    loan.save((err, loanDB) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!loanDB) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            loan: loanDB
        });
    });
}


module.exports = app;