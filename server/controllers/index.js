const express = require('express');

const app = express();

app.use(require('./userController'));
app.use(require('./loanController'));

module.exports = app;