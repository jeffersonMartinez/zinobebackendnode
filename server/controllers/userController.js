const express = require('express');

const app = express();

const User = require('../model/user');

app.get('/user', async (req, res) => {
    User.find({}, (err, user) => {        
        responsePost(res, err, user);
    })
});

app.get('/user/:documentId', async (req, res) => {
    console.log('sssssssssssss');
    console.log(req.params);
    User.findOne({ documentId: req.params.documentId}, (err, user) => {        
        responsePost(res, err, user);
    })
});

app.post('/user', async (req, res) => {
    let body = req.body;

    console.log(body);

    let user = await User.findOne({ documentId: body.documentId });

    if (!user) {
        user = new User({
            name: body.name,
            email: body.email,
            documentId: body.documentId,
        });
    }
    user.loan.push(body.loan);
    
    user.save((err, userDB) => {
        responsePost(res, err, userDB);
    });
});

const responsePost = (res, err, user) => {
    console.log(user);
    console.log(err);
    if (err) {
        return res.status(500).json({
            ok: false,
            err
        });
    }

    res.json({
        ok: true,
        user
    });
}


module.exports = app;