/**
 * Server port
 */
process.env.PORT = process.env.PORT || 3000;

/**
 * Mongo database url
 */
process.env.URLDB = 'mongodb://mongoadmin:secret@192.168.99.100:27017/bankLoans?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false';
// process.env.URLDB = 'mongodb://192.168.99.100:27017/BankLoans';
